/*
 * License GNU LGPL
 * Copyright (C) 2012 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.control;

import com.panemu.tiwulfx.common.TiwulFXUtil;
import com.panemu.tiwulfx.form.NumberControl;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

/**
 *
 * @author Amrullah <amrullah@panemu.com>
 */
public class NumberField<T extends Number> extends TextField {

    private DecimalFormat formatter = TiwulFXUtil.getDecimalFormat();
    private boolean ignore;
    private int maxLength = 10;
    private ObjectProperty<T> value = new SimpleObjectProperty<>();
    private String regex = "";
	private ObjectProperty<Class<T>> clazzProperty = new SimpleObjectProperty<>();
    private boolean grouping = false;
	private String MAX_VALUE_STRING = "";
   private int DECIMAL_DIGIT = 2;

    public NumberField() {
        this((Class<T>) Double.class);
    }

    public NumberField(Class<T> clazz) {
        this.clazzProperty.set(clazz);
        textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String s, String s1) {
                if (ignore) {
                    return;
                }
                ignore = true;
                if (maxLength > -1 && s1.length() > maxLength) {
                    setText(s1.substring(0, maxLength));
                }
                checkDecimal(getText());
                if (getRegex() != null && !getRegex().equals("") && !s1.matches(getRegex())) {
                    setText(s);
                }
                if (getText().isEmpty() || getText().equals(formatter.getDecimalFormatSymbols().getDecimalSeparator())) {
                    setText("");
                    value.set(null);
                } else if (clazzProperty.get() != BigDecimal.class && (s1.length() > MAX_VALUE_STRING.length() 
					|| (s1.length() == MAX_VALUE_STRING.length() && s1.compareTo(MAX_VALUE_STRING) > 0))) {
                    setText(s);
                } else {
					value.set(castToExpectedType(getText()));
				}
                ignore = false;
            }
        });
        focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
                formatter.applyPattern(getPattern(!newValue && grouping));
                ignore = true;
                if (value.get() != null) {
                    setText(formatter.format(value.get()));
                } else {
                    setText("");
                }
                ignore = false;
            }
        });
        if (clazz != null) {
            setupRegex();
        }

        value.addListener(new ChangeListener<T>() {
            @Override
            public void changed(ObservableValue<? extends T> observable, T oldValue, T newValue) {
                ignore = true;
                if (newValue != null) {
                    formatter.applyPattern(getPattern(isGrouping()));
                    setText(formatter.format(newValue));
                } else {
                    setText("");
                }
                ignore = false;
            }
        });
		clazzProperty.addListener(new ChangeListener<Class<T>>() {

			@Override
			public void changed(ObservableValue<? extends Class<T>> ov, Class<T> t, Class<T> t1) {
				setupRegex();
		        formatter.setParseBigDecimal(t1.equals(BigDecimal.class));
				resetMaxValueString();
			}
		});
		
		resetMaxValueString();
    }
	
    private void checkDecimal(String angka) {
       int idx = angka.indexOf(separator);
       if (idx >= 0) {
         String dec = angka.substring(idx + 1, angka.length());
         if (dec.length() > DECIMAL_DIGIT) {
            String header = angka.substring(0, idx);
            String digitDec = dec.substring(0,2);
            String accepted = header + separator + digitDec;
            setText(accepted);
         }
      }
    }
    
	private void resetMaxValueString() {
		if(clazzProperty.get() == Long.class) {
			MAX_VALUE_STRING = String.valueOf(Long.MAX_VALUE);
		} else if (clazzProperty.get() == Integer.class) {
			MAX_VALUE_STRING = String.valueOf(Integer.MAX_VALUE);
		} else if (clazzProperty.get() == Double.class) {
			MAX_VALUE_STRING = String.valueOf(Double.MAX_VALUE);
		} else if (clazzProperty.get() == Float.class) {
			MAX_VALUE_STRING = String.valueOf(Float.MAX_VALUE);
		} else {
			MAX_VALUE_STRING = "";
		}
	}

    protected String getRegex() {
        return regex;
    }

    private char separator;
    private void setupRegex() {
        if (clazzProperty.get().equals(Integer.class) || clazzProperty.get().equals(Long.class)) {
            regex = "-?\\d*";
            return;
        }
        separator = TiwulFXUtil.getDecimalFormat().getDecimalFormatSymbols().getDecimalSeparator();
        if (separator == '.') {
            regex = "-?\\d*\\.?\\d*";
        } else {
            regex = "-?\\d*" + separator + "?\\d*";
        }
    }

    public final void setNumberType(Class<T> clazz) {
        this.clazzProperty.set(clazz);
    }
	
	public final ObjectProperty<Class<T>> numberTypeProperty() {
		return this.clazzProperty;
	}

    /**
     * Cast string to Number type that accepted by this NumberField. It depends
     * on NumberType (see {@link #setNumberType}
     *
     * @param numberString
     * @return
     */
    public T castToExpectedType(String numberString) {
        if (numberString.length() == 1 && numberString.charAt(0) == formatter.getDecimalFormatSymbols().getDecimalSeparator()) {
            return null;
        }
        try {
            Number number = formatter.parse(numberString);
            T val = (T) number;

            if (clazzProperty.get().equals(BigDecimal.class) && !(number instanceof BigDecimal)) {
                if (number instanceof Long || number instanceof Integer) {
					val =(T) BigDecimal.valueOf(number.longValue());
                } else {
					val =(T) BigDecimal.valueOf(number.doubleValue());
                }
            } else if (clazzProperty.get().equals(Double.class) && !(number instanceof Double)) {
                val = (T) new Double(number.doubleValue());
            } else if (clazzProperty.get().equals(Float.class) && !(number instanceof Float)) {
                val = (T) new Float(number.floatValue());
            } else if (clazzProperty.get().equals(Integer.class) && !(number instanceof Integer)) {
                val = (T) new Integer(number.intValue());
            }

            return val;
        } catch (ParseException ex) {
            throw new RuntimeException(ex);
        }
    }

	/**
	 * Set whether it will use thousand separator or not
	 * @param grouping set it to true to use thousand separator
	 */
    public void setGrouping(boolean grouping) {
        this.grouping = grouping;
        formatter.applyPattern(getPattern(grouping));
    }

	/**
	 * 
	 * @return true if the NumberField display thousand separator
	 */
    public boolean isGrouping() {
        return grouping;
    }

    protected String getPattern(boolean grouping) {
        if (clazzProperty.get().equals(Integer.class) || clazzProperty.get().equals(Long.class)) {
            if (grouping) {
                formatter.setParseBigDecimal(clazzProperty.get().equals(BigDecimal.class));
                return "###,###";
            } else {
                return "###";
            }
        } else {
            if (grouping) {
                formatter.setParseBigDecimal(clazzProperty.get().equals(BigDecimal.class));
                return "###,##0.00";
            } else {
                return "##0.00";
            }
        }
    }

    public ObjectProperty<T> valueProperty() {
        return value;
    }

    public void setValue(T value) {
        this.value.set(value);
    }

    public T getValue() {
        return value.get();
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public int getMaxLength() {
        return maxLength;
    }
}
