/*
 * License GNU LGPL
 * Copyright (C) 2012 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.control;

import javafx.beans.property.StringProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.LabelBuilder;
import javafx.scene.layout.HBox;
import javafx.scene.layout.HBoxBuilder;
import javafx.scene.layout.StackPane;

public class LabelSeparator extends StackPane {

    private Label lblText;

    public LabelSeparator(String label) {
        this(label, true);
    }

    public LabelSeparator(String label, boolean topPading) {
        HBox line = HBoxBuilder.create()
                .styleClass("line")
                .minHeight(2)
                .prefHeight(2)
                .prefWidth(USE_PREF_SIZE)
                .maxHeight(USE_PREF_SIZE)
                .build();
        if (topPading) {
            setPadding(new Insets(10, 0, 0, 0));
        }
        lblText = LabelBuilder.create().text(label).build();
        this.getChildren().addAll(line, lblText);
        this.getStyleClass().add("label-separator");

    }

    public void setText(String label) {
        textProperty().set(label);
    }

    public String getText() {
        return textProperty().get();
    }

    public StringProperty textProperty() {
        return lblText.textProperty();
    }
}
