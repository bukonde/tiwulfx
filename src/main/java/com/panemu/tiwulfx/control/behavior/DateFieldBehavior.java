/*
 * License GNU LGPL
 * Copyright (C) 2012 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.control.behavior;

import com.panemu.tiwulfx.control.DateField;
import com.sun.javafx.scene.control.behavior.BehaviorBase;
import com.sun.javafx.scene.control.behavior.KeyBinding;
import com.sun.javafx.scene.control.behavior.TextInputControlBindings;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author Amrullah <amrullah@panemu.com>
 */
public class DateFieldBehavior extends BehaviorBase<DateField> {

    public static final String ACTION_SHOW_SUGGESTION = "togglePopup";
    public static final String ACTION_ADD_DAY = "addDay";
    public static final String ACTION_ADD_MONTH = "addMonth";
    public static final String ACTION_ADD_YEAR = "addYear";
    public static final String ACTION_SUBSTRACT_DAY = "subtractDay";
    public static final String ACTION_SUBSTRACT_MONTH = "subtractMonth";
    public static final String ACTION_SUBSTRACT_YEAR = "subtractYear";
    protected static final List<KeyBinding> KEY_BINDINGS = new ArrayList<KeyBinding>();
    static {
        KEY_BINDINGS.add(new KeyBinding(KeyCode.SPACE, ACTION_SHOW_SUGGESTION).ctrl());
        KEY_BINDINGS.add(new KeyBinding(KeyCode.UP, ACTION_ADD_DAY));
        KEY_BINDINGS.add(new KeyBinding(KeyCode.UP, ACTION_ADD_MONTH).ctrl());
        KEY_BINDINGS.add(new KeyBinding(KeyCode.UP, ACTION_ADD_YEAR).ctrl().shift());
        KEY_BINDINGS.add(new KeyBinding(KeyCode.DOWN, ACTION_SUBSTRACT_DAY));
        KEY_BINDINGS.add(new KeyBinding(KeyCode.DOWN, ACTION_SUBSTRACT_MONTH).ctrl());
        KEY_BINDINGS.add(new KeyBinding(KeyCode.DOWN, ACTION_SUBSTRACT_YEAR).ctrl().shift());
        // However, we want to consume other key press / release events too, for
        // things that would have been handled by the InputCharacter normally
        KEY_BINDINGS.add(new KeyBinding(null, KeyEvent.KEY_PRESSED, "Consume"));
    }
    
    public DateFieldBehavior(DateField c) {
        super(c);
    }
    
    @Override
    protected List<KeyBinding> createKeyBindings() {
//        KEY_BINDINGS.get(0).
        return KEY_BINDINGS;
    }

    @Override
    protected void callAction(String actionName) {
        if (ACTION_SHOW_SUGGESTION.equals(actionName)) {
            if (getControl().isShowing()) {
                getControl().hideCalendar();
            } else {
                getControl().showCalendar();
            }
        } else if (ACTION_ADD_DAY.equals(actionName)) {
            increaseDay();
        } else if (ACTION_ADD_MONTH.equals(actionName)) {
            increaseMonth();
        } else if (ACTION_ADD_YEAR.equals(actionName)) {
            increaseYear();
        } else if (ACTION_SUBSTRACT_DAY.equals(actionName)) {
            decreaseDay();
        }else if (ACTION_SUBSTRACT_MONTH.equals(actionName)) {
            decreaseMonth();
        } else if (ACTION_SUBSTRACT_YEAR.equals(actionName)) {
            decreaseYear();
        }
    }
    
    private void increaseMonth() {
        changeDate(Calendar.MONTH, 1);
    }
    
    private void decreaseMonth() {
        changeDate(Calendar.MONTH, -1);
    }
    
    private void increaseDay() {
        changeDate(Calendar.DATE, 1);
    }
    
    private void decreaseDay() {
        changeDate(Calendar.DATE, -1);
    }
    
    private void increaseYear() {
        changeDate(Calendar.YEAR, 1);
    }
    
    private void decreaseYear() {
        changeDate(Calendar.YEAR, -1);
    }
    
    private void changeDate(int dayOrMonthOrYear, int offset) {
        Date date = getControl().getSelectedDate();
        if (date == null) {
            date = new Date();
        }
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        calendar.add(dayOrMonthOrYear, offset);
		  Date candidateDate = calendar.getTime();
		 if (getControl().getController() == null || getControl().getController().isEnabled(candidateDate)) {
			 getControl().setSelectedDate(candidateDate);
		 }
		
    }
    
}
