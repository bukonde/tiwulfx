/*
 * License GNU LGPL
 * Copyright (C) 2013 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.table;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

/**
 * This column doesn't support sorting. Use {@link AdvanceButtonColumn} for more advance feature
 * @author Amrullah <amrullah@panemu.com>
 * @param <T>
 */
public class ButtonColumn<T> extends TableColumn<T, Object> {

	public static interface ButtonColumnHelper<S> {

		String getLabel(S record);

		void handle(S record);
	}
	private ButtonColumnHelper<T> helper;

	public ButtonColumn() {
		this(new ButtonColumnHelper<T>() {
			@Override
			public String getLabel(T record) {
				return "undefined";
			}

			@Override
			public void handle(T record) {
				throw new RuntimeException("Please define the helper for ButtonColumn");
			}
		});
	}

	public ButtonColumn(ButtonColumnHelper<T> helper) {
		super();
		this.helper = helper;
		setSortable(false);
		setCellFactory(new Callback<TableColumn<T, Object>, TableCell<T, Object>>() {
			@Override
			public TableCell<T, Object> call(TableColumn<T, Object> param) {
				return new TickCell();
			}
		});

	}

	public ButtonColumnHelper<T> getHelper() {
		return helper;
	}

	public void setHelper(ButtonColumnHelper<T> helper) {
		this.helper = helper;
	}

	private class TickCell extends TableCell<T, Object> {

		private EventHandler<ActionEvent> buttonHandler = new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent t) {
				helper.handle((T) getTableRow().getItem());
			}
		};
		private Button button = new Button();

		public TickCell() {
			super();
			setGraphic(button);
			button.setAlignment(Pos.CENTER);
			setText(null);
			setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
			button.setMaxWidth(Double.MAX_VALUE);
			button.setOnAction(buttonHandler);
			contentDisplayProperty().addListener(new ChangeListener<ContentDisplay>() {
				private boolean suspendEvent = false;

				@Override
				public void changed(ObservableValue<? extends ContentDisplay> observable, ContentDisplay oldValue, ContentDisplay newValue) {
					if (suspendEvent) {
						return;
					}
					if (newValue != ContentDisplay.GRAPHIC_ONLY) {
						suspendEvent = true;
						setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
						suspendEvent = false;
					}
				}
			});

		}

		@Override
		protected void updateItem(Object item, boolean empty) {
			boolean emptyRow = getTableView().getItems().size() < getIndex() + 1;
			super.updateItem(item, empty && emptyRow);
			if (!emptyRow) {
				setGraphic(button);
				if (getTableRow() != null) {
					button.setText(helper.getLabel((T) getTableRow().getItem()));
				}
			} else {
				setGraphic(null);
			}
		}
	}
}
