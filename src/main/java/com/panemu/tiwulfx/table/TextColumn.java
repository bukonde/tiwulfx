/*
 * License GNU LGPL
 * Copyright (C) 2012 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.table;

import com.panemu.tiwulfx.common.TableCriteria.Operator;
import com.panemu.tiwulfx.common.TiwulFXUtil;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 *
 * @author amrullah
 */
public class TextColumn<S extends Object> extends BaseColumn<S, String> {

	private TextField searchInputControl = new TextField();
	private boolean emptyStringAsNull = TiwulFXUtil.DEFAULT_EMPTY_STRING_AS_NULL;
	
	private SearchMenuItemBase<String> searchMenuItem = new SearchMenuItemBase<String>(this) {
		@Override
		protected Node getInputControl() {
			searchInputControl.setPromptText(TiwulFXUtil.getLiteral("enter.keyword"));
			return searchInputControl;
		}

		@Override
		protected List<Operator> getOperators() {
			List<Operator> lst = new ArrayList<>();
			lst.add(Operator.eq);
			lst.add(Operator.ilike_begin);
			lst.add(Operator.ilike_anywhere);
			lst.add(Operator.ilike_end);
			lst.add(Operator.is_null);
			lst.add(Operator.is_not_null);
			return lst;
		}

		@Override
		protected String getValue() {
			return searchInputControl.getText();
		}
	};

	public TextColumn() {
		this("");
	}

	public boolean isEmptyStringAsNull() {
		return emptyStringAsNull;
	}

	public void setEmptyStringAsNull(boolean emptyStringAsNull) {
		this.emptyStringAsNull = emptyStringAsNull;
	}

	public TextColumn(String propertyName) {
		this(propertyName, 100);
	}

	public TextColumn(String propertyName, double preferredWidth) {
		super(propertyName, preferredWidth);

		Callback<TableColumn<S, String>, TableCell<S, String>> cellFactory =
				new Callback<TableColumn<S, String>, TableCell<S, String>>() {
			@Override
			public TableCell call(TableColumn p) {
				return new TextTableCell();
			}
		};

		setCellFactory(cellFactory);
		setStringConverter(stringConverter);
	}

	@Override
	MenuItem getSearchMenuItem() {
		searchInputControl.setText(getDefaultSearchValue());
		return searchMenuItem;
	}

	public class TextTableCell extends BaseCell<S, String> {

		private TextField textField;

		public TextTableCell() {
			super(getStringConverter());
			this.setAlignment(TextColumn.this.getAlignment());
		}

		@Override
		protected void setValueToEditor(String value) {
			textField.setText(value);
		}

		@Override
		protected String getValueFromEditor() {
			if (textField.getText() == null 
					|| (isEmptyStringAsNull() && textField.getText().trim().isEmpty()) ){
				return null;
			}
			return textField.getText();
		}

		@Override
		protected Control getEditor() {
			if (textField == null) {
				textField = new TextField();
				textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
				textField.textProperty().addListener(new ChangeListener<String>() {

					@Override
					public void changed(ObservableValue<? extends String> ov, String t, String newValue) {
						for (CellEditorListener<S, String> svl : getCellEditorListeners()) {
							svl.valueChanged(TextTableCell.this, getTableRow().getIndex(), getPropertyName(), (S) getTableRow().getItem(), newValue);
						}
					}
				});
				
			}
			return textField;
		}
	}
	private StringConverter<String> stringConverter = new StringConverter<String>() {
		@Override
		public String toString(String t) {
			if (t == null || (isEmptyStringAsNull() && t.trim().isEmpty())) {
				return getNullLabel();
			}
			return t;
		}

		@Override
		public String fromString(String string) {
			if (string == null || string.equals(getNullLabel())
					|| (isEmptyStringAsNull() && string.isEmpty())) {
				return null;
			}
			return string;
		}
	};
}
