/*
 * Copyright (C) 2013 Panemu.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package com.panemu.tiwulfx.table;

import com.panemu.tiwulfx.common.TableCriteria.Operator;
import com.panemu.tiwulfx.control.TypeAheadField;
import com.panemu.tiwulfx.control.skin.TypeAheadFieldSkin;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 *
 * @author amrullah
 */
public class TypeAheadColumn<S, T> extends BaseColumn<S, T>{
	private ObservableMap<String, T> itemMap = FXCollections.observableMap(new LinkedHashMap<String, T>());
	
	private TypeAheadField<T> searchInputControl = new TypeAheadField<>();
	private SearchMenuItemBase<T> searchMenuItem = new SearchMenuItemBase<T>(this) {
		@Override
		protected Node getInputControl() {
			return searchInputControl;
		}

		@Override
		protected List<Operator> getOperators() {
			List<Operator> lst = new ArrayList<>();
			lst.add(Operator.eq);
			lst.add(Operator.ne);
			lst.add(Operator.is_null);
			lst.add(Operator.is_not_null);
			return lst;
		}

		@Override
		protected T getValue() {
			return searchInputControl.getValue();
		}
	};
	
	public TypeAheadColumn() {
		this("");
	}

	public TypeAheadColumn(String propertyName) {
		this(propertyName, 100);
	}

	public TypeAheadColumn(String propertyName, double prefWidth) {
		super(propertyName, prefWidth);
		setCellFactory(new Callback<TableColumn<S, T>, TableCell<S, T>>() {
			@Override
			public TableCell<S, T> call(TableColumn<S, T> param) {
				return new TypeAheadTableCell();
			}
		});
		searchInputControl.setConverter(stringConverter);
		searchInputControl.setFocusTraversable(false);

		setStringConverter(stringConverter);
	}
	
	@Override
	MenuItem getSearchMenuItem() {
		if (getDefaultSearchValue() != null) {
			searchInputControl.setValue(getDefaultSearchValue());
		}
		return searchMenuItem;
	}
	
	/**
	 * Add pair of label and object corresponding to the label
	 *
	 * @param label
	 * @param object
	 */
	public void addItem(String label, T object) {
		itemMap.put(label, object);
		searchInputControl.getItems().add(object);
	}

	public void removeItem(String label) {
		searchInputControl.getItems().remove(itemMap.get(label));
		itemMap.remove(label);
	}
	public void clearItems() {
		searchInputControl.getItems().clear();
		itemMap.clear();
	}
	
	private StringConverter<T> stringConverter = new StringConverter<T>() {
		@Override
		public String toString(T object) {
			if (object == null) {
				return getNullLabel();
			}
			Set<String> keys = itemMap.keySet();
			for (String label : keys) {
				T obj = itemMap.get(label);
				if (obj.equals(object)) {
					return label;
				}
			}
			return object.toString();
		}

		@Override
		public T fromString(String string) {
			if (string == null || string.equals(getNullLabel())) {
				return null;
			}
			return itemMap.get(string);
		}
	};
	
	private BooleanProperty sortedProperty = new SimpleBooleanProperty(false);
	/**
	 * 
	 * @return 
	 * @see #setSorted(boolean) 
	 */
	public boolean isSorted() {
		return sortedProperty.get();
	}
	
	/**
	 * Set whether items are sorted alphabetically. By Default is false which means the order of the items
	 * is based on the order they are registered to TypeAheadField.
	 * @param sorted set to true to sort the items. Default is false.
	 */
	public void setSorted(boolean sorted) {
		sortedProperty.set(sorted);
	}
	
	/**
	 * Property of sorted attribute
	 * @return 
	 * @see #setSorted(boolean) 
	 */
	public BooleanProperty sortedProperty() {
		return sortedProperty;
	}
	
	public class TypeAheadTableCell extends BaseCell<S, T> {
		
		private TypeAheadField<T> typeAheadField;

		public TypeAheadTableCell() {
			super(getStringConverter());
		}

		@Override
		protected void setValueToEditor(T value) {
			typeAheadField.setValue(value);
		}

		@Override
		protected T getValueFromEditor() {
			return typeAheadField.getValue();
		}

		/**
		 * 
		 * @return the textfield in TypeAhead
		 */
		@Override
		protected Control getFocusableControl() {
			if (typeAheadField.getSkin() != null) {
				return ((TypeAheadFieldSkin<T>) typeAheadField.getSkin()).getTextField();
			}
			return null;
		}
		
		@Override
		protected Control getEditor() {
			if (typeAheadField == null) {
				typeAheadField = new TypeAheadField<>();
				
				typeAheadField.sortedProperty().bind(TypeAheadColumn.this.sortedProperty);
				if (!isRequired()) {
					typeAheadField.getItems().add(null);
				}
				
				typeAheadField.valueProperty().addListener(new ChangeListener<T>() {

					@Override
					public void changed(ObservableValue<? extends T> ov, T t, T newValue) {
						for (CellEditorListener<S,T> svl : getCellEditorListeners()) {
							svl.valueChanged(TypeAheadTableCell.this, getTableRow().getIndex(), getPropertyName(), (S) getTableRow().getItem(), newValue);
						}
					}
				});
				
				requiredProperty().addListener(new ChangeListener<Boolean>() {
					@Override
					public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
						if (t1) {
							typeAheadField.getItems().remove(null);
						} else {
							typeAheadField.getItems().add(0, null);
						}
					}
				});
				typeAheadField.getItems().addAll(itemMap.values());
				itemMap.addListener(new MapChangeListener<String, T>() {
					@Override
					public void onChanged(MapChangeListener.Change<? extends String, ? extends T> change) {
						if (change.wasAdded()) {
							typeAheadField.getItems().add(change.getValueAdded());
						} else {
							typeAheadField.getItems().remove(change.getValueRemoved());
						}
					}
				});

				typeAheadField.setConverter(stringConverter);

			}
			return typeAheadField;
		}
		
		@Override
		protected void attachEnterEscapeEventHandler() {
			/**
			 * Use event filter instead on onKeyPressed because Enter and Escape
			 * have been consumed by TypeAhead it self
			 */
			typeAheadField.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
				@Override
				public void handle(KeyEvent t) {
					if (t.getCode() == KeyCode.ENTER) {
						commitEdit(typeAheadField.getValue());
						t.consume();
					} else if (t.getCode() == KeyCode.ESCAPE) {
						cancelEdit();
						/**
						 * Propagate ESCAPE key press to cell
						 */
						TypeAheadTableCell.this.fireEvent(t);
					}
				}
			});
		}
	}
	
}
