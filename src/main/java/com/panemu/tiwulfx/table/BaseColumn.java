/*
 * License GNU LGPL
 * Copyright (C) 2012 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.table;

import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TiwulFXUtil;
import com.panemu.tiwulfx.common.Validator;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PopupControl;
import javafx.scene.control.Skin;
import javafx.scene.control.Skinnable;
import javafx.scene.control.TableColumn;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.HBoxBuilder;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.apache.commons.beanutils.PropertyUtils;

/**
 * This is a parent class for columns that display value from a POJO object.
 * There are two variations of concrete column implementation in regards with
 * cell editor:
 * <ul>
 * <li>Cell editor is a single control: TextField, CheckBox</li>
 * <li>Cell editor is a composite of controls: ComboBox, DateField, LookupField.
 * They are composed from a TextField and a button
 * </ul>
 *
 * Please refer to {@link TextColumn} and {@link ComboBoxColumn} source code to
 * get reference on building your own column.
 *
 * Column that doesn't extend BaseColumn, i.e: {@link TickColumn} will be
 * skipped by export-to-excel and paste routine
 *
 * @author amrullah
 */
public class BaseColumn<S, T> extends TableColumn<S, T> {

	/**
	 * used to get/set object method using introspection
	 */
	private String propertyName;
	private final SimpleObjectProperty<TableCriteria> tableCriteria = new SimpleObjectProperty<>();
	private T searchValue;
	private ImageView filterImage = new ImageView(new Image(BaseColumn.class.getResourceAsStream("/images/filter.png")));
	protected Pos alignment = Pos.CENTER_LEFT;
	private ObservableMap<S, String> mapInvalid = FXCollections.observableHashMap();
	private List<Validator<T>> lstValidator = new ArrayList<>();
	private String nullLabel = TiwulFXUtil.DEFAULT_NULL_LABEL;
	private Map<S, RecordChange<S, T>> mapChangedRecord = new HashMap<>();

	private StringConverter<T> stringConverter = new StringConverter<T>() {

		@Override
		public String toString(T t) {
			if (t == null) {
				return nullLabel;
			} else {
				return t.toString();
			}
		}

		@Override
		public T fromString(String string) {
			throw new UnsupportedOperationException(BaseColumn.class.getName() + ". The implementation class of BaseColum should provide string converter by calling setStringConverter() in constructor.");
		}
	};

	/**
	 *
	 * @param propertyName java bean property name to be used for get/set method
	 * using introspection
	 * @param prefWidth preferred column width
	 */
	public BaseColumn(String propertyName, double prefWidth) {
		this(propertyName, prefWidth, TiwulFXUtil.getLiteral(propertyName));
	}

	/**
	 *
	 * @param propertyName java bean property name to be used for get/set method
	 * using introspection
	 * @param prefWidth preferred collumn width
	 * @param columnHeader column header text. Default equals propertyName. This
	 * text is localized
	 */
	public BaseColumn(String propertyName, double prefWidth, String columnHeader) {
		super(columnHeader);
		setPrefWidth(prefWidth);
		this.propertyName = propertyName;
//        setCellValueFactory(new PropertyValueFactory<S, T>(propertyName));
		tableCriteria.addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				if (tableCriteria.get() != null) {
					BaseColumn.this.setGraphic(filterImage);
				} else {
					BaseColumn.this.setGraphic(null);
				}
			}
		});
		setCellValueFactory(new Callback<CellDataFeatures<S, T>, ObservableValue<T>>() {
			private SimpleObjectProperty<T> propertyValue;

			@Override
			public ObservableValue<T> call(CellDataFeatures<S, T> param) {
				/**
				 * This code is adapted from {@link PropertyValueFactory#getCellDataReflectively(T)
				 */
				try {
					Object cellValue;
					if (getPropertyName().contains(".")) {
						cellValue = PropertyUtils.getNestedProperty(param.getValue(), getPropertyName());
					} else {
						cellValue = PropertyUtils.getSimpleProperty(param.getValue(), getPropertyName());
					}
					propertyValue = new SimpleObjectProperty<>((T) cellValue);
					return propertyValue;
				} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
					throw new RuntimeException(ex);
				} catch (Exception ex) {
					/**
					 * Ideally it catches
					 * org.apache.commons.beanutils.NestedNullException. However
					 * we need to import apachec bean utils library in FXML file
					 * to be able to display it in Scene Builder. So, I decided
					 * to catch Exception to avoid the import.
					 */
					return new SimpleObjectProperty<>(null);
				}
			}
		});
	}

	public StringConverter<T> getStringConverter() {
		return this.stringConverter;
	}
	
	public void setStringConverter(StringConverter<T> stringConverter) {
		this.stringConverter = stringConverter;
	}

	/**
	 * Property that holds applied criteria to column
	 *
	 * @return
	 */
	public SimpleObjectProperty<TableCriteria> tableCriteriaProperty() {
		return tableCriteria;
	}

	public String getNullLabel() {
		return nullLabel;
	}

	public void setNullLabel(String nullLabel) {
		this.nullLabel = nullLabel;
	}
	
	/**
	 * Get criteria applied to this column
	 *
	 * @return
	 * @see #tableCriteriaProperty()
	 */
	public TableCriteria getTableCriteria() {
		return tableCriteria.get();
	}

	/**
	 * Set criteria to be applied to column. If you are going to set criteria to
	 * multiple columns, it is encouraged to call {@link TableControl#setReloadOnCriteriaChange(boolean)
	 * }
	 * and pass FALSE as parameter. It will disable autoreload on criteria
	 * change. After assign all criteria, call
	 * {@link TableControl#reloadFirstPage()}. You might want to set {@link TableControl#setReloadOnCriteriaChange(boolean)
	 * } back to true after that.
	 *
	 * @param crit
	 * @see TableControl#setReloadOnCriteriaChange(boolean)
	 */
	public void setTableCriteria(TableCriteria crit) {
		tableCriteria.set(crit);
	}

	/**
	 * Gets propertyName passed in constructor
	 *
	 * @return
	 */
	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	/**
	 * Get Search Menu Item that is displayed in Table context menu
	 *
	 * @return
	 */
	MenuItem getSearchMenuItem() {
		return null;
	}

	void setDefaultSearchValue(T searchValue) {
		this.searchValue = searchValue;
	}

	T getDefaultSearchValue() {
		return searchValue;
	}

	/**
	 * Gets cell alignment
	 *
	 * @return
	 */
	public Pos getAlignment() {
		return alignment;
	}

	/**
	 * Sets cell alignment
	 *
	 * @param alignment
	 */
	public void setAlignment(Pos alignment) {
		this.alignment = alignment;
	}
	private BooleanProperty filterable = new SimpleBooleanProperty(true);

	public BooleanProperty filterableProperty() {
		return filterable;
	}

	/**
	 * Specifies whether right clicking the column will show menu item to do
	 * filtering. If filterable is true, search menu item will be displayed in
	 * context menu.
	 */
	public void setFilterable(boolean filterable) {
		this.filterable.set(filterable);
	}

	public boolean isFilterable() {
		return this.filterable.get();
	}
	private BooleanProperty required = new SimpleBooleanProperty(false);

	/**
	 * Set the field to required and cannot null. Some columns implementation
	 * provide empty value that user can select if the column is not required.
	 *
	 * @param required
	 */
	public void setRequired(boolean required) {
		this.required.set(required);
	}

	public boolean isRequired() {
		return required.get();
	}

	public BooleanProperty requiredProperty() {
		return required;
	}

	/**
	 * Convert
	 * <code>stringValue</code> to value that is acceptable by this column.
	 * Avoid to override this method, override
	 * {@link #convertFromString_Impl(java.lang.String) convertFromString_Impl}
	 * instead. This method will throw runtime exception if stringValue
	 * parameter is not empty but the conversion result is null. In case of
	 * CheckBoxColumn, this method is overridden because CheckBoxColumn has
	 * nullLabel for null value.
	 *
	 * @param stringValue
	 * @return
	 */
	public final T convertFromString(String stringValue) {
		return stringConverter.fromString(stringValue);
	}

	/**
	 * Convert
	 * <code>value</code> to String as represented in TableControl
	 *
	 * @param value
	 * @return
	 */
	public final String convertToString(T value) {
		return stringConverter.toString(value);
	}

	/**
	 * Set the value displayed in this column for specified record to valid. To
	 * set it to invalid call {@link #setInvalid}
	 */
	public void setValid(S record) {
		mapInvalid.remove(record);
	}

	/**
	 * Set the value displayed in this column for specified record to invalid.
	 *
	 * @see #setValid()
	 * @param invalidMessage
	 */
	public void setInvalid(S record, String invalidMessage) {
		mapInvalid.put(record, invalidMessage);
	}

	/**
	 * Check whether specified record's value that displayed in this column is
	 * valid.
	 *
	 * @param record
	 * @return true for valid
	 */
	public boolean isValid(S record) {
		return !mapInvalid.containsKey(record);
	}

	/**
	 * Add validator. The validator will be called with the same sequence the
	 * validators are added to input controls. One validator instance is
	 * reusable across columns, but only can be added once in a column.
	 *
	 * @param validator
	 */
	public void addValidator(Validator<T> validator) {
		if (!lstValidator.contains(validator)) {
			lstValidator.add(validator);
		}
	}

	public void removeValidator(Validator<T> validator) {
		lstValidator.remove(validator);
	}

	/**
	 * Validate value contained in the input control. To make the input control
	 * mandatory, call {@link #setRequired(boolean true)}
	 *
	 * @return false if invalid. True otherwise
	 * @see #addValidator(com.panemu.tiwulfx.common.Validator) to add validator
	 */
	public boolean validate(S record) {
		T value = getCellData(record);
		if (required.get() && (value == null || (value instanceof String && value.toString().trim().length() == 0))) {
			String msg = TiwulFXUtil.getLiteral("field.mandatory");
			setInvalid(record, msg);
			return false;
		}

		//do not trim
		if (value instanceof String && ((String) value).length() == 0) {
			value = null;
		}
		if (value != null) {
			for (Validator<T> validator : lstValidator) {
				String msg = validator.validate(value);
				if (msg != null && !"".equals(msg.trim())) {
					setInvalid(record, msg);
					return false;
				}
			}
		}
		setValid(record);
		return true;
	}
	private PopupControl popup;
	Label errorLabel = new Label();

	PopupControl getPopup(S record) {
		String msg = mapInvalid.get(record);
		if (popup == null) {
			popup = new PopupControl();
			final HBox pnl = HBoxBuilder.create().children(errorLabel).build();
			pnl.getStyleClass().add("error-popup");
			popup.setSkin(new Skin() {
				@Override
				public Skinnable getSkinnable() {
					return null;
				}

				@Override
				public Node getNode() {
					return pnl;
				}

				@Override
				public void dispose() {
				}
			});
			popup.setHideOnEscape(true);
		}
		errorLabel.setText(msg);
		return popup;
	}
	
	public void addRecordChange(S record, T oldValue, T newValue) {
        if (mapChangedRecord.containsKey(record)) {
            RecordChange<S, T> rc = mapChangedRecord.get(record);
            rc.setNewValue(newValue);
        } else {
            RecordChange<S, T> rc = new RecordChange<>(record, getPropertyName(), oldValue, newValue);
            mapChangedRecord.put(record, rc);
        }
    }

    public void revertRecordChange(S record) {
        RecordChange rc = mapChangedRecord.get(record);
        if (rc != null) {
            try {
                PropertyUtils.setSimpleProperty(record, getPropertyName(), rc.getOldValue());
                mapChangedRecord.remove(record);
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
                throw new RuntimeException(ex);
            }
        }
    }
    
    public void clearRecordChange() {
        mapChangedRecord.clear();
    }
	
	Map<S, RecordChange<S, T>> getRecordChangeMap() {
		return mapChangedRecord;
	}
	
	private List<CellEditorListener<S,T>> lstValueChangeListener = new ArrayList<CellEditorListener<S, T>>();
	
	protected List<CellEditorListener<S,T>> getCellEditorListeners() {
		return lstValueChangeListener;
	}
	
	/**
	 * Listen to cell editor value's change before it is committed to table cell
	 * @param selectedValueListener 
	 */
	public void addCellEditorListener(CellEditorListener<S,T> selectedValueListener) {
		if (!lstValueChangeListener.contains(selectedValueListener)) {
			lstValueChangeListener.add(selectedValueListener);
		}
	}
	
	public void removeCellEditorListener(CellEditorListener<S,T> selectedValueListener) {
		lstValueChangeListener.remove(selectedValueListener);
	}
}
