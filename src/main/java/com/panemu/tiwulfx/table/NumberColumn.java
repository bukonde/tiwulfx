/*
 * License GNU LGPL
 * Copyright (C) 2012 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.table;

import com.panemu.tiwulfx.common.TableCriteria.Operator;
import com.panemu.tiwulfx.common.TiwulFXUtil;
import com.panemu.tiwulfx.control.NumberField;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 *
 * @author amrullah
 */
public class NumberColumn<S, T extends Number> extends BaseColumn<S, T> {

	private boolean grouping = true;
	private int maxLength = 10;
	private ObjectProperty<Class<T>> clazzProperty = new SimpleObjectProperty<>();
	private NumberField<T> searchInputControl = new NumberField<>(null);
	protected String pattern = "###,###";
	protected DecimalFormat formatter = TiwulFXUtil.getDecimalFormat();
	private SearchMenuItemBase<T> searchMenuItem = new SearchMenuItemBase<T>(this) {
		@Override
		protected Node getInputControl() {
			return searchInputControl;
		}

		@Override
		protected List<Operator> getOperators() {
			List<Operator> lst = new ArrayList<>();
			lst.add(Operator.eq);
			lst.add(Operator.ne);
			lst.add(Operator.lt);
			lst.add(Operator.le);
			lst.add(Operator.gt);
			lst.add(Operator.ge);
			lst.add(Operator.is_null);
			lst.add(Operator.is_not_null);
			return lst;
		}

		@Override
		protected T getValue() {
			return searchInputControl.getValue();
		}
	};

	public NumberColumn() {
		this("", (Class<T>) Double.class);
	}

	public NumberColumn(String propertyName, Class<T> clazz) {
		this(propertyName, clazz, 100);
	}

	public NumberColumn(String propertyName, Class<T> clazz, double prefWidth) {
		super(propertyName, prefWidth);
		setAlignment(Pos.CENTER_RIGHT);
		this.clazzProperty.set(clazz);
		Callback<TableColumn<S, T>, TableCell<S, T>> cellFactory =
				new Callback<TableColumn<S, T>, TableCell<S, T>>() {
			@Override
			public TableCell call(TableColumn p) {
				return new NumberTableCell();
			}
		};
		setCellFactory(cellFactory);
		formatter.setParseBigDecimal(clazz.equals(BigDecimal.class));
		formatter.applyPattern(getPattern(grouping));
		searchInputControl.numberTypeProperty().bind(this.clazzProperty);
		this.clazzProperty.addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable o) {
				formatter.applyPattern(getPattern(grouping));
			}
		});
		setStringConverter(stringConverter);
	}

	/**
	 * 
	 * @return true if the NumberField display thousand separator
	 */
	public boolean isGrouping() {
		return grouping;
	}

	@Override
	MenuItem getSearchMenuItem() {
		if (getDefaultSearchValue() != null) {
			searchInputControl.setValue(getDefaultSearchValue());
		}
		return searchMenuItem;
	}

	/**
	 * Set whether it will use thousand separator or not
	 * @param grouping set it to true to use thousand separator
	 */
	public void setGrouping(boolean grouping) {
		this.grouping = grouping;
		pattern = getPattern(grouping);
		formatter.applyPattern(pattern);
	}
	private StringConverter<T> stringConverter = new StringConverter<T>() {
		@Override
		public String toString(T value) {
			return value == null ? getNullLabel() : formatter.format(value);
		}

		@Override
		public T fromString(String stringValue) {
			if (stringValue == null || stringValue.equals(getNullLabel())) {
				return null;
			}
			return searchInputControl.castToExpectedType(stringValue);
		}
	};

	/**
	 * Set maximum character length is acceptable in input field
	 *
	 * @param maxLength
	 */
	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public int getMaxLength() {
		return maxLength;
	}

	protected String getPattern(boolean grouping) {
		if (clazzProperty.get().equals(Integer.class) || clazzProperty.get().equals(Long.class)) {
			if (grouping) {
				formatter.setParseBigDecimal(clazzProperty.get().equals(BigDecimal.class));
				return "###,###";
			} else {
				return "###";
			}
		} else {
			if (grouping) {
				formatter.setParseBigDecimal(clazzProperty.get().equals(BigDecimal.class));
				return "###,##0.00";
			} else {
				return "##0.00";
			}
		}
	}

	public Class<T> getNumberType() {
		return clazzProperty.get();
	}

	public void setNumberType(Class<T> numberType) {
		this.clazzProperty.set(numberType);
	}

	public class NumberTableCell extends BaseCell<S, T> {

		private NumberField<T> textField;

		public NumberTableCell() {
			super(getStringConverter());
			this.setAlignment(NumberColumn.this.getAlignment());
		}

		@Override
		protected void setValueToEditor(T value) {
			textField.setValue(value);
		}

		@Override
		protected T getValueFromEditor() {
			return textField.getValue();
		}

		@Override
		protected Control getEditor() {
			if (textField == null) {
				textField = new NumberField<>(clazzProperty.get());
				textField.setMaxLength(maxLength);
				textField.valueProperty().addListener(new ChangeListener<T>() {

					@Override
					public void changed(ObservableValue<? extends T> ov, T t, T newValue) {
						for (CellEditorListener<S,T> svl : getCellEditorListeners()) {
							svl.valueChanged(NumberTableCell.this, getTableRow().getIndex(), getPropertyName(), (S) getTableRow().getItem(), newValue);
						}
					}
				});
			}
			return textField;
		}

		@Override
		public void cancelEdit() {
			super.cancelEdit();
		}
	}
}
