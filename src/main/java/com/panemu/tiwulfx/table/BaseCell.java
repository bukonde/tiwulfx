/*
 * License GNU LGPL
 * Copyright (C) 2012 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.table;

import com.sun.javafx.css.StyleManager;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Control;
import javafx.scene.control.PopupControl;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.StringConverter;

/**
 *
 * @author Amrullah <amrullah@panemu.com>
 */
public abstract class BaseCell<S, T> extends TableCell<S, T> {

	private Control control;
	private StringConverter<T> stringConverter;
	public BaseCell(StringConverter<T> stringConverter) {
		this.stringConverter = stringConverter;
		contentDisplayProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				if (control == null) {
					control = getEditor();
					attachEnterEscapeEventHandler();
					attachFocusListener();
					setGraphic(control);
				}
				setValueToEditor(getItem());
			}
		});

		/**
		 * commit change on lost focus
		 */
		this.selectedProperty().addListener(new InvalidationListener() {
			@Override
			public void invalidated(Observable observable) {
				if (isEditing()) {
					commitEdit(getValueFromEditor());
				}
			}
		});

		addEventHandler(MouseEvent.ANY, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (event.getEventType() == MouseEvent.MOUSE_EXITED || event.getEventType() == MouseEvent.MOUSE_MOVED) {
					TableColumn<S, T> clm = getTableColumn();
					if (clm instanceof BaseColumn && !((BaseColumn) clm).isValid(getTableRow().getItem())) {
						BaseColumn<S, T> baseColumn = (BaseColumn<S, T>) clm;
						PopupControl popup = baseColumn.getPopup((S) getTableRow().getItem());
						if (event.getEventType() == MouseEvent.MOUSE_MOVED
								&& !popup.isShowing()) {

							Point2D p = BaseCell.this.localToScene(0.0, 0.0);
							popup.show(BaseCell.this,
									p.getX() + getScene().getX() + getScene().getWindow().getX(),
									p.getY() + getScene().getY() + getScene().getWindow().getY() + BaseCell.this.getHeight() - 1);
						} else if (event.getEventType() == MouseEvent.MOUSE_EXITED && popup.isShowing()) {
							popup.hide();
						}
					}
				}
			}
		});
	}
	
	/**
	 * For the case of TypeAhead, Date and Lookup, the focusable control is the textfield, not
	 * the control itself. This method is to be overridden by TypeAheadTableCell, DateTableCell and LookupTableCell
	 * 
	 * @return 
	 */
	protected Control getFocusableControl() {
		return control;
	}

	private void attachFocusListener() {
		/**
		 * Set cell mode to edit if the editor control receives focus. This is
		 * intended to deal with mouse click. This way, commitEdit() will be
		 * called if the cell is no longer focused
		 */
		if (getTableRow().getItem() != null) {
			Runnable runnable = new Runnable() {
				public void run() {
					if (getFocusableControl() == null) {
						/**
						 * Hopefully, the processor has its time to instantiate the focusable control so we can stop
						 * calling this method recursively
						 */
						attachFocusListener();
					} else {
//						try {
						getFocusableControl().focusedProperty().addListener(new ChangeListener<Boolean>() {
							@Override
							public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
								if (!BaseCell.this.isSelected() && newValue) {
									getTableView().getSelectionModel().select(getIndex(), getTableColumn());
								}
								if (!isEditing() && newValue) {
									programmaticallyEdited = true;
									getTableView().edit(getIndex(), getTableColumn());
									programmaticallyEdited = false;
								}
							}
						});
//						} catch (Exception ex) {
//							System.out.println("------------------------------------------------------");
//							System.out.println("error message: " + ex.getMessage());
//							System.out.println("row item: " + getTableRow().getItem());
//							System.out.println("row index: " + getTableRow().getIndex());
//							System.out.println("item: " + getItem());
//							System.out.println("index: " + getIndex());
//
//						}
					}
				}
			};
			Platform.runLater(runnable);
		}
	}
	private boolean programmaticallyEdited = false;

	@Override
	public void startEdit() {
		/**
		 * If a row is added, new cells are created. The old cells are not
		 * disposed automatically. They still respond to user event's.
		 * Fortunately, the "should-be-discarded" cells have invisible row so we
		 * can recognize them and prevent them to interact with user's event.
		 */
		if (!this.getTableRow().isVisible() || !getTableRow().isEditable()) {
			return;
		}
		super.startEdit();
		if (!programmaticallyEdited) {

			if (control == null) {
				control = getEditor();
				attachEnterEscapeEventHandler();
				attachFocusListener();
			}
			setGraphic(control);
			setValueToEditor(getItem());
			setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
			/**
			 * put focus on the textfield so user can directly typed on it
			 */
			Runnable r = new Runnable() {
				@Override
				public void run() {
					control.requestFocus();
				}
			};
			Platform.runLater(r);
		}
	}

	protected abstract void setValueToEditor(T value);

	protected abstract T getValueFromEditor();

	protected abstract Control getEditor();

	@Override
	public void commitEdit(T newValue) {
		super.commitEdit(newValue);
		updateItem(newValue, false);
	}

	@Override
	public void cancelEdit() {
		setValueToEditor(getItem());
		super.cancelEdit();
		Runnable r = new Runnable() {
			@Override
			public void run() {
				getTableView().requestFocus();
			}
		};
		Platform.runLater(r);
	}

	@Override
	public void updateItem(T item, boolean empty) {
		boolean emptyRow = getTableView().getItems().size() < getIndex() + 1;
		/**
		 * don't call super.updateItem() because it will trigger cancelEdit() if
		 * the cell is being edited. It causes calling commitEdit() ALWAYS call
		 * cancelEdit as well which is undesired.
		 *
		 */
		if (!isEditing()) {
			super.updateItem(item, empty && emptyRow);
		}
		if (empty && isSelected()) {
			updateSelected(false);
		}
		if (empty && emptyRow) {
			setText(null);
			//do not nullify graphic here. Let the TableRow to control cell dislay
		} else if (!isEditing()) {
			setText(getString(item));
		}
	}

	protected final String getString(T value) {
		return stringConverter.toString(value);
	}

	protected void attachEnterEscapeEventHandler() {
		control.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent t) {
				if (t.getCode() == KeyCode.ENTER) {
					commitEdit(getValueFromEditor());
				} else if (t.getCode() == KeyCode.ESCAPE) {
					cancelEdit();
				}
			}
		});
	}
	
	@Override
	public long impl_getPseudoClassState() {
		long mask = super.impl_getPseudoClassState();
		TableColumn<S, T> clm = getTableColumn();
		if (clm instanceof BaseColumn && getTableRow() != null && !((BaseColumn) clm).isValid(getTableRow().getItem())) {
			mask |= StyleManager.getInstance().getPseudoclassMask("invalid");
		}
		return mask;
	}
}
