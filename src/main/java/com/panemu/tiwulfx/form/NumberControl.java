/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.panemu.tiwulfx.form;

import com.panemu.tiwulfx.control.NumberField;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.TextField;

/**
 *
 * @author hahach
 */
public class NumberControl<T extends Number> extends BaseControl<T, NumberField<T>> {

    private NumberField<T> numberField;
    public NumberControl() {
        this((Class<T>) Double.class);
    }

    public NumberControl(Class<T> clazz) {
        super(new NumberField(clazz));
        numberField = getInputComponent();
    }

    @Override
    protected void bindValuePropertyWithControl(NumberField inputControl) {
        value.bind(inputControl.valueProperty());
    }

    @Override
    public void setValue(T value) {
        numberField.setValue(value);
    }

    @Override
    protected void bindEditablePropertyWithControl(NumberField<T> inputControl) {
        inputControl.editableProperty().bind(editableProperty());
    }

    public final void setNumberType(Class<T> clazz) {
        numberField.setNumberType(clazz);
    }

    public void setGrouping(boolean grouping) {
        numberField.setGrouping(grouping);
    }

    public boolean isGrouping() {
        return numberField.isGrouping();
    }

    public void setMaxLength(int maxLength) {
        numberField.setMaxLength(maxLength);
    }

    public int getMaxLength() {
        return numberField.getMaxLength();
    }

    public final String getText() {
        return numberField.getText();
    }

}
