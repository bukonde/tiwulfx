/*
 * Copyright (C) 2013 Panemu.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package com.panemu.tiwulfx.form;

import com.panemu.tiwulfx.control.TypeAheadField;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.util.StringConverter;

/**
 *
 * @author amrullah
 */
public class TypeAheadControl<T> extends BaseControl<T, TypeAheadField<T>> {
	private TypeAheadField<T> typeAheadField;
	public TypeAheadControl() {
		this("");
	}

	public TypeAheadControl(String propertyName) {
		super(propertyName, new TypeAheadField<T>());
		typeAheadField = getInputComponent();
	}

	public TypeAheadControl(String propertyName, TypeAheadField control) {
		super(propertyName, control);
	}

	@Override
	protected void bindValuePropertyWithControl(TypeAheadField inputControl) {
		value.bind(inputControl.valueProperty());
	}

	@Override
	public void setValue(T value) {
		typeAheadField.setValue(value);
	}

	public final ObservableList<T> getItems() {
		return typeAheadField.getItems();
	}

	public void addItem(String Label, T value) {
		typeAheadField.addItem(Label, value);
	}

	public final StringConverter<T> getConverter() {
		return typeAheadField.getConverter();
	}

	public String getPromptText() {
		return typeAheadField.getPromptText();
	}

	public void setPromptText(String promptText) {
		typeAheadField.setPromptText(promptText);
	}

	public StringProperty promptTextProperty() {
		return typeAheadField.promptTextProperty();
	}
	
	
	
}
