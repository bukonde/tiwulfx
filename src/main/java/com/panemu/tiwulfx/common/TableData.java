/*
 * License GNU LGPL
 * Copyright (C) 2012 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.common;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author amrullah
 */
public class TableData<T> implements Serializable {
    private List<T> rows;
    private boolean moreRows;
    private long totalRows;

    public TableData(List<T> rows, boolean moreRows, long totalRows) {
        this.rows = rows;
        this.moreRows = moreRows;
        this.totalRows = totalRows;
    }
    
    public List<T> getRows() {
        return rows;
    }

    public boolean isMoreRows() {
        return moreRows;
    }

    public long getTotalRows() {
        return totalRows;
    }
    
    
}
