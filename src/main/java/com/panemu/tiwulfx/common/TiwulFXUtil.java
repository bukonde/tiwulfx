/*
 * License GNU LGPL
 * Copyright (C) 2012 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.common;

import com.panemu.tiwulfx.table.TableControl;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.scene.control.Control;
import javafx.scene.control.Tooltip;

/**
 *
 * @author Amrullah <amrullah@panemu.com>
 */
public class TiwulFXUtil {

	/**
	 * TODO get default locale from configuration file
	 */
	private static Locale loc = new Locale("en", "US");
	private static DateFormat dateFormat;
	private static DateFormat dateTimeFormat;
	private static ResourceBundle literalBundle = ResourceBundle.getBundle("literal", loc);
	public static int DEFAULT_LOOKUP_SUGGESTION_ITEMS = 10;
	public static int DEFAULT_LOOKUP_SUGGESTION_WAIT_TIMES = 500;
	public static String DEFAULT_NULL_LABEL = "";
	public static boolean DEFAULT_EMPTY_STRING_AS_NULL = true;
	public static String DEFAULT_DATE_PROMPTEXT = "";
	private static String applicationId = ".tiwulfx";
	
	/**
	 * Set global default value for {@link TableControl#useBackgrounTask}
	 * property. If it is set to true, TableControl will use background task to
	 * execute Load, Save, Delete and Export actions. Default value is false. Set
	 * {@link TableControl#setUseBackgroundTask(boolean)} to change the default
	 * for individual TableControl.
	 */
	public static boolean DEFAULT_USE_BACKGROUND_TASK = false;
	
	/**
	 * Default number of rows displayed in
	 * {@link com.panemu.tiwulfx.table.TableControl TableControl}
	 */
	public static int DEFAULT_TABLE_MAX_ROW = 500;

	public static Locale getLocale() {
		return loc;
	}
	
	/**
	 * Application id will be the folder name where the configuration file is
	 * stored inside user's home folder. The folder will be started with a dot.
	 *
	 * @param id
	 */
	public static void setApplicationId(String id) {
		if (id != null && !id.contains(File.separator) && !id.contains(" ")) {
			applicationId = id;
		} else {
			throw new IllegalArgumentException(id + " is invalid application ID. It should not contains space and " + File.separator);
		}
		if (!applicationId.startsWith(".")) {
			applicationId = "." + applicationId;
		}
	}
	
	public static String getApplicationId() {
		return applicationId;
	}

	public static DateFormat getDateFormat() {
		if (dateFormat == null) {
			dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, getLocale());
			dateFormat.setLenient(false);
		}
		return dateFormat;
	}

	public static void setDateFormat(DateFormat dateFormat) {
		TiwulFXUtil.dateFormat = dateFormat;
		if (dateFormat instanceof SimpleDateFormat
			&& (DEFAULT_DATE_PROMPTEXT == null || DEFAULT_DATE_PROMPTEXT.length() == 0)) {
			DEFAULT_DATE_PROMPTEXT = ((SimpleDateFormat) dateFormat).toPattern();
		}
	}

	public static DateFormat getDateTimeFormat() {
		if (dateTimeFormat == null) {
			dateTimeFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, getLocale());
			dateTimeFormat.setLenient(false);
		}
		return dateTimeFormat;
	}

	public static void setDateTimeFormat(DateFormat dateTimeFormat) {
		TiwulFXUtil.dateTimeFormat = dateTimeFormat;
	}

	public static void setLocale(Locale loc) {
		TiwulFXUtil.loc = loc;
		literalBundle = ResourceBundle.getBundle("literal", loc);
	}

	public static DecimalFormat getDecimalFormat() {
		NumberFormat nf = NumberFormat.getNumberInstance(loc);
		return (DecimalFormat) nf;
	}

	public static ResourceBundle getLiteralBundle() {
		return literalBundle;
	}

	public static void setLiteralBundle(ResourceBundle literalBundle) {
		TiwulFXUtil.literalBundle = literalBundle;
	}

	public static String getLiteral(String key) {
		try {
			return literalBundle.getString(key);
		} catch (Exception ex) {
			return key;
		}

	}

	public static String getLiteral(String key, Object... param) {
		try {
			return MessageFormat.format(literalBundle.getString(key), param);
		} catch (Exception ex) {
			return key;
		}

	}

	public static void setToolTip(Control control, String key) {
		control.setTooltip(new Tooltip(getLiteral(key)));
	}

	public static void openFile(String fileToOpen) throws Exception {
		Desktop.getDesktop().open(new File(fileToOpen));
	}
	private static String OS = System.getProperty("os.name").toLowerCase();

	public static boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}

	public static boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}

	public static boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	}

	public static boolean isSolaris() {
		return (OS.indexOf("sunos") >= 0);
	}

	private static String getConfigurationPath() throws IOException {
		String home = System.getProperty("user.home");
		
		String confPath = home + File.separator + applicationId;
		
		File confFile = new File(confPath);
		if (!confFile.exists()) {
			confFile.mkdirs();
		}
		confPath = home + File.separator + applicationId + File.separator + "conf.properties";
		confFile = new File(confPath);
		if (!confFile.exists()) {
			confFile.createNewFile();
		}
		return confPath;
	}
	
	private static Properties loadProperties() {
		Properties p = new Properties();
		try {
			FileInputStream in = new FileInputStream(getConfigurationPath());
			p.load(in);
			in.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return p;

	}
	
	private static Properties confFile;
	private static Properties getConfigurations() {
		if (confFile == null) {
			confFile = loadProperties();
		}
		return confFile;
	}

	public static String readProperty(String propName) {
		return getConfigurations().getProperty(propName);
	}
	
	public synchronized static void deleteProperties(List<String> propNames) throws Exception {
		for (String propName : propNames) {
			getConfigurations().remove(propName);
		}
		writePropertiesToFile();
	}
	
	public synchronized static void deleteProperties(String propName) throws Exception {
		getConfigurations().remove(propName);
		writePropertiesToFile();
	}

	/**
	 * 
	 * @param mapPropertyValue
	 *          propertyName, Value
	 * @return
	 */
	public synchronized static void writeProperties(Map<String, String> mapPropertyValue) throws Exception {
		Set<String> propNames = mapPropertyValue.keySet();
		for (String propName : propNames) {
			String value = mapPropertyValue.get(propName);
			getConfigurations().setProperty(propName, value);
		}
		writePropertiesToFile();
	}

	private synchronized static void writePropertiesToFile() {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(getConfigurationPath());
			getConfigurations().store(out, "Last updated: " + (new Date()));
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	public synchronized static void writeProperties(String propName, String value) throws Exception {
		getConfigurations().setProperty(propName, value);
		writePropertiesToFile();
	}
}
